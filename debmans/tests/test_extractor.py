#!/usr/bin/python3
# coding: utf-8

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import logging
import re
import subprocess

import pytest

from debmans.extractor import PackageMirror, PackageExtractor
from debmans.renderer import DefaultManpageRenderer
from debmans.utils import mkdirp

#: packages to test extraction on
PACKAGES = ['man-db', 'linux-image-amd64', 'gnutls-bin']

#: number of manpages in said packages
MANPAGES_COUNT = 137


@pytest.fixture(scope='session')
def fake_repo(tmpdir_factory):
    tmpdir = tmpdir_factory.mktemp('repo')
    tmpdir.join('pool').mkdir()
    origdir = tmpdir.join('pool').chdir()
    # one package that has a manpage, one that doesn't
    #
    # other small packages candidates with no manpages:
    #
    # php5
    # python3-args
    # libboost-dev
    #
    # man was chosen because it's a complicated manpage, gnutls-bin
    # was chosen because man2html was failing with it
    files = []
    for package in PACKAGES:
        output = subprocess.check_output(['dget', package])
        # running twice because only the second one gives output
        output = subprocess.check_output(['dget', package])
        match = re.search(r'dget: using existing (.*\.deb)', output)
        files.append(match.group(1))
    tmpdir.join('dists').mkdir()
    distdir = tmpdir.join('dists', 'jessie')
    distdir.mkdir()
    distdir.join('main').mkdir()
    distdir.join('main', 'binary-all').mkdir()
    tmpdir.chdir()
    pkgs_str = subprocess.check_output(['dpkg-scanpackages', 'pool/'])
    origdir.chdir()
    with distdir.join('Release').open('w') as relfile:
        relfile.write('''Origin: Debian
Label: Debian
Suite: stable
Version: 8.6
Codename: jessie''')
    with distdir.join('main', 'binary-all', 'Packages').open('w') as pkgfile:
        pkgfile.write(pkgs_str)
    return tmpdir


def test_extractor(fake_repo, tmpdir):
    logging.info('extracting files from %s', fake_repo)
    mirror = PackageMirror(str(fake_repo))
    extractor = PackageExtractor([DefaultManpageRenderer.pattern],
                                 root=str(fake_repo), destdir=str(tmpdir))
    for suite, pkg in mirror.packages:
        logging.info("found package: %s", pkg.get('Filename'))
        assert pkg.get('Filename') is not None
        extractor.extract(pkg)
        if 'man' in pkg.get('Filename'):
            assert len(extractor.files) == MANPAGES_COUNT
        logging.debug("found manpages: %s", extractor.files)
    assert len(mirror.packages) == len(PACKAGES)
    return extractor.files, mirror.packages


def test_cache(fake_repo, tmpdir):
    mp = test_extractor(fake_repo, tmpdir)
    assert len(mp) > 0, "extractor doesn't work"
    assert len(list(tmpdir.join('.cache').visit())) == len(PACKAGES)
    with pytest.raises(AssertionError):
        mp = test_extractor(fake_repo, tmpdir)
        assert len(mp) == 0


def test_mirror(fake_repo, tmpdir):
    mirror = PackageMirror(str(fake_repo))
    assert 'jessie' in mirror.releases
    assert mirror.releases['jessie'] == '8.6 jessie (stable)'
