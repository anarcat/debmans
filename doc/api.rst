API
===

This is the API documentation of Debmans. It should be stable across
major releases. See the :doc:`design` document for more details about
the design.

Extractor
---------

.. why can't this be just the __doc__ of the renderer module (ie. the
   top of file docstring)? somehow it fails to show up then...

The extractor processes Debian packages and extracts specific patterns
into a target directory. It uses a cache file that is named according
to the package name and version to avoid the costly operation of
opening the same package file multiple times.

.. automodule:: debmans.extractor
   :members:

.. note:: the documentation for :mod:`~click` functions is
          incomplete. they should actually be turned into usage page
          and manpages, see `this issue
          <https://github.com/pallets/click/issues/381>`_ for details.

.. autofunction:: debmans.extractor.extract

Renderer
--------

The Renderer module takes care of turning extracted documentation into
HTML format. It uses Jinja templates and simple timestamp-based
caching.

.. automodule:: debmans.renderer
   :members:

.. autofunction:: debmans.renderer.render

.. autofunction:: debmans.renderer.site

Search
------

The search module takes care of indexing and searching manpages. It
currently has very limited functionality.

.. automodule:: debmans.search
   :members:

.. autofunction:: debmans.search.serve

Main entry point
----------------

The main entry point of ``debmans`` is in the :mod:`debmans.__main__`
module. This is to make it possible to call ``debmans`` directly from
the source code through the Python interpreter with::

  python -m debmans

All this code is here rather than in ``__init__.py`` to avoid
requiring too many dependencies in the base module, which contains
useful metadata for ``setup.py``.

This uses the :mod:`~click` module to define the base command and
options, which then get passed to subcommands through the ``obj``
parameter, see :func:`~click.pass_obj` in the click documentation.

.. automodule:: debmans.__main__
   :members:

.. autofunction:: debmans.__main__.debmans

Logger
------

This is a simple helper module to configure the :mod:`logging` module
consistently.

.. automodule:: debmans.logger
   :members:

Utilities
---------

Those are various utilities reused in multiple modules that did not
fit anywhere else.

.. automodule:: debmans.utils
   :members:

