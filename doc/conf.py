# -*- coding: utf-8 -*-
#
# debmans documentation build configuration file, created by
# sphinx-quickstart on Fri Nov 18 16:37:08 2016.
#
# This file is execfile()d with the current directory set to its
# containing dir.
import os
import sys
sys.path.insert(0, os.path.abspath('..'))

extensions = [
    'sphinx.ext.todo',      # .. todo:: items
    'sphinx.ext.autodoc',   # parse API docs
    'sphinx.ext.intersphinx',
    'sphinx.ext.coverage',  # check for documentation coverage
    'sphinx.ext.viewcode',  # show code samples
]

# sort API documentation by source, not alphabetically
autodoc_member_order = 'bysource'
todo_include_todos = True

templates_path = ['_templates']

source_suffix = '.rst'

# The master toctree document.
master_doc = 'index'

# General information about the project.
project = u'debmans'
copyright = u'2016, Antoine Beaupré'
author = u'Antoine Beaupré'

# The short X.Y version.
from debmans import version
# The full version, including alpha/beta/rc tags.
release = version

# default language, no translation in docs
language = None
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
pygments_style = 'sphinx'

html_theme = 'default'

# ignore apt module imports so docs build without it
# see also https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=845330
autodoc_mock_imports = ['apt', 'apt.debfile', 'debian' ]

# on_rtd is whether we are on readthedocs.org, this line of code
# grabbed from docs.readthedocs.org
on_rtd = os.environ.get('READTHEDOCS', None) == 'True'

# would prefer alabaster theme because it is visually lighter, but it
# doesn't support arbitrary source links:
# https://github.com/bitprophet/alabaster/issues/87
if not on_rtd:  # only import and set the theme if we're building docs locally
    try:
        import sphinx_rtd_theme
        html_theme = 'sphinx_rtd_theme'
        html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
    except ImportError:  # nosec
        pass
# otherwise, readthedocs.org uses their theme by default, so no need
# to specify it

# RTD: show source and link to our site
html_context = {
    'source_url_prefix': "https://gitlab.com/anarcat/debmans/blob/HEAD/doc/",
    'source_suffix': '.rst',
}

intersphinx_mapping = {
    'click': ('http://click.pocoo.org/', None),
    'jinja': ('http://jinja.pocoo.org/docs/', None),
    'python': ('https://docs.python.org/3/', None),
}

# other output formats than HTML
latex_documents = [
    (master_doc, 'debmans.tex', u'debmans Documentation',
     author, 'manual'),
]
man_pages = [
    (master_doc, 'debmans', u'debmans Documentation',
     [author], 1)
]
epub_title = project
epub_author = author
epub_publisher = author
epub_copyright = copyright
epub_exclude_files = ['search.html']
