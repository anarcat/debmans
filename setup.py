#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

import io
import os
import os.path
import re
import sys
sys.path.insert(0, os.path.dirname(__file__))

import debmans


def read(*names, **kwargs):
    return io.open(
        os.path.join(os.path.dirname(__file__), *names),
        encoding=kwargs.get('encoding', 'utf8')
    ).read()

setup(name="debmans",
      author="Antoine Beaupré",
      author_email="anarcat@debian.org",
      description=debmans.__description__,
      long_description=re.sub(':[a-z]+:`~?(.*?)`', r'``\1``', read('README.rst')),
      platforms='Debian',
      license=debmans.__license_short__,
      url=debmans.__website__,
      use_scm_version={
          'write_to': 'debmans/_version.py',
      },
      packages=["debmans"],
      package_data={
          'debmans': ['static/*.mdwn',
                      'static/*.css',
                      'static/*.js',
                      'static/template.html',
                      'static/Pics/*'],
      },
      entry_points={
          "console_scripts":
          [
              "debmans = debmans.__main__:main",
          ]
      },
      setup_requires=['setuptools_scm',
                      'pytest-runner',
                      'sphinx',
                      ],
      install_requires=[
          "click",
          "python-debian",
          "humanize",
          "jinja2",
          "markdown",
          "pyliblzma",
      ],
      extras_require={
          "dev": [
              "pytest",
              "tox",
              "pyflakes",
          ],
          "debian": [
              # upstream refuses to update PyPI:
              # https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=845330
              "python-apt",
          ],
      },
      tests_require=['pytest'],
      classifiers=[
          'Development Status :: 5 - Production/Stable',
          'Environment :: Console',
          'Environment :: Web Environment',
          'Intended Audience :: End Users/Desktop',
          'Intended Audience :: Developers',
          'Intended Audience :: System Administrators',
          'Intended Audience :: Information Technology',
          'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
          'Natural Language :: English',
          'Operating System :: POSIX :: Linux',
          'Programming Language :: Python',
          'Programming Language :: Python :: 2',
          'Programming Language :: Python :: 2.7',
          'Programming Language :: Python :: 2 :: Only',
          'Topic :: Documentation',
          'Topic :: Internet :: WWW/HTTP :: Indexing/Search',
          'Topic :: Software Development :: Libraries :: Python Modules',
          'Topic :: System :: Archiving :: Packaging',
          'Topic :: Text Processing :: Markup :: HTML',
          ],
      )
